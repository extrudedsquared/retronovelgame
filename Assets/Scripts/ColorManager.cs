using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    GameObject[] userInterfaceElements;
    GameObject userInterfaceElement;
    ColorPicker colorPicker;

    private Color[] retroGreen = { new Color(0f, 0.3764706f, 0.04313726f), new Color(0f, 0.8113208f, 0.09296378f) };
    private Color[] yeeOrange = { new Color(0.6509804f, 0.3490196f, 0f), new Color(1f, 0.5333334f, 0f) };

    private Color[] currentColor;

    /*private string[] Green = new string[] { "00600B", "00CF18"};
    private string[] Orange = new string[] { "A65900", "FF8800" };
    private string[] Blue = new string[] { "006193", "00A7FF" };
    */
    // Start is called before the first frame update

    // Start is called before the first frame update
    void Awake()
    {
        currentColor = retroGreen;
    }

    public void setColors(int colorInt)
    {
       colorIntChooser(colorInt); //sets the current color
       ColorPicker[] userInterfaceElements = GameObject.FindObjectsOfType<ColorPicker>();
        for (int i = 0; i < userInterfaceElements.Length; i++)
        {
            colorPicker = userInterfaceElements[i];
            colorPicker.SetColor(currentColor);
        }
    }
    public Color[] getCurrentColor() { return currentColor; }


    private void colorIntChooser(int colorInt)
    {

        switch (colorInt)
        {
            case 0:
                currentColor = retroGreen;
                break;
            case 1:
                currentColor = yeeOrange;
                break;
            default:
                currentColor = retroGreen;
                break;
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    
}
