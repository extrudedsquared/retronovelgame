using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class YarnCommandManager : MonoBehaviour
{
    [SerializeField] private Image characterImage;
    [SerializeField] private Image mainImage;
    [SerializeField] private Text text;

    [SerializeField] private Sprite FailImage;

    [Header("Test Character Sprites")]
    [SerializeField] private Sprite Fox;
    [SerializeField] private Sprite Man;
    [SerializeField] private Sprite Goblin;

    [Header("Test Main Screen Sprites")]
    [SerializeField] private Sprite Food;
    [SerializeField] private Sprite Town;
    [SerializeField] private Sprite Bar;




    // Start is called before the first frame update
    [YarnCommand("updateCharImage")]
    public void updateCharImage(string spriteName)
    {
        characterImage.sprite = spriteChooser(spriteName);
    }


    [YarnCommand("updateMainImage")]
    public void updateMainImage(string spriteName)
    {
        mainImage.sprite = spriteChooser(spriteName);
    }


    private Sprite spriteChooser(string spriteName)
    {
        Sprite chosenSprite;
        switch (spriteName.ToLower())
        {
            default: chosenSprite = FailImage; break;
            case "fox": chosenSprite = Fox; break;
            case "man": chosenSprite = Man; break;
            case "goblin": chosenSprite = Man; break;

            case "food": chosenSprite = Food; break;
            case "town": chosenSprite = Town; break;
            case "bar": chosenSprite = Bar; break;
        }
        return chosenSprite;
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
}
