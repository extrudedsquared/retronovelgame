using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour
{

    //Test

    Outline hasOutline;
    Button hasButton;
    Text hasText;
    Image hasImage;
    void Start()
    {
        hasOutline = this.GetComponent<Outline>();
        hasButton = this.GetComponent<Button>();
        hasText = this.GetComponent<Text>();
        hasImage = this.GetComponent<Image>();

        ColorManager colorManager = GameObject.FindObjectOfType<ColorManager>();
        SetColor(colorManager.getCurrentColor());
        print(colorManager.getCurrentColor()[0]);

    }

    public void SetColor(Color[] currentColor) 
    {
        if (hasOutline)
        {
            var colors = hasOutline.effectColor;
            colors = currentColor[0];
            hasOutline.effectColor = colors;
        }
        if (hasButton)
        {

            var colors = hasButton.colors;
            colors.normalColor = currentColor[0];
            colors.highlightedColor = currentColor[1];
            hasButton.colors = colors;

        }
        if(hasText)
        {
            var colors = hasText.color;
            colors = currentColor[0];
            hasText.color = colors;

        }
        if(hasImage && !hasButton)
        {
            var colors = hasImage.color;
            colors = currentColor[1];
            hasImage.color = colors;
        } 
    }

    // Update is called once per frame




    
}
